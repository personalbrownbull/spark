# spark-submit 01_filter.py
from pyspark import SparkConf, SparkContext
import collections
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/1800.csv"

def parseLine(line):
  fields = line.split(',')
  stationID = fields[0]
  entryType = fields[2]
  tempF = float(fields[3]) * 0.1 * (9.0/5.0) + 32.0
  return (stationID, entryType, tempF)

# Dbg
dbg = 1

# Spark Config
conf = SparkConf().setMaster("local").setAppName("MinTemperatures")
sc = SparkContext(conf = conf)

# INPUT
lines = sc.textFile(inputFile)
''' i.e.
ITE00100554,18000101,TMAX,-75,,,E,
ITE00100554,18000101,TMIN,-148,,,E,
GM000010962,18000101,PRCP,0,,,E,
'''

## PARSE
rdd = lines.map(parseLine)
if dbg: printRDDHead(rdd, stage = "rdd")
''' i.e.
rdd[0]: ('ITE00100554', 'TMAX', 18.5)
rdd[1]: ('ITE00100554', 'TMIN', 5.359999999999999)
'''

## FILTER
### TMAX entryType ONLY
minTemps = rdd.filter(lambda x: "TMAX" in x[1])
if dbg: printRDDHead(minTemps, stage = "minTemps")
''' i.e.
minTemps[0]: ('ITE00100554', 'TMAX', 5.359999999999999)
minTemps[1]: ('EZE00100082', 'TMAX', 7.699999999999999)
'''

## REMAP/DROP entryType col
stationTemps = minTemps.map(lambda x: (x[0], x[2]))
if dbg: printRDDHead(stationTemps, stage = "stationTemps")
''' i.e.
stationTemps[0]: ('ITE00100554', 5.359999999999999)
stationTemps[1]: ('EZE00100082', 7.699999999999999)
'''

## AGGREGATE MIN by station
minTempsAgg = stationTemps.reduceByKey(lambda x, y: max(x,y))
if dbg: printRDDHead(minTempsAgg, stage = "minTempsAgg")
''' i.e.
minTempsAgg[0]: ('ITE00100554', 5.359999999999999)
minTempsAgg[1]: ('EZE00100082', 7.699999999999999)
'''

# OUTPUT
results = minTempsAgg.collect()
if dbg: printRDDHead(results, stage = "results")
''' i.e.
results[0]: ('ITE00100554', 5.359999999999999)
results[1]: ('EZE00100082', 7.699999999999999)
'''

for result in results:
  print(result[0] + "\t{:.2f}F".format(result[1]))