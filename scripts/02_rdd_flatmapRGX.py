# spark-submit 02_flatmapRGX.py
from pyspark import SparkConf, SparkContext
import collections
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/Book.txt"

# FUNCS
import re
def normalizeWords(text):
  return re.compile(r'\W+', re.UNICODE).split(text.lower())

# Dbg
dbg = 1

# Spark Config
conf = SparkConf().setMaster("local").setAppName("Wordcount")
sc = SparkContext(conf = conf)

# INPUT
lines = sc.textFile(inputFile)
# if dbg: printRDDHead(lines, stage = "lines")
# ''' i.e.
# '''

# PARSE
# words = lines.flatMap(lambda x: x.split()) # old way with out punctuation filtering
words = lines.flatMap(normalizeWords)
if dbg: printRDDHead(words, stage = "words")
''' i.e.
words[0]: self
words[1]: employment
'''

# COUNT by word
wordCounts = words.countByValue()
if dbg: printRDDHead(wordCounts, stage = "wordCounts")
''' i.e.
wordCounts[0]: self
wordCounts[1]: employment
'''

# OUTPUT
if dbg: printRDDHead(wordCounts.items(), stage = "wordCounts.items()")
''' i.e.
wordCounts.items()[0]: ('self', 111)
wordCounts.items()[1]: ('employment', 75)
'''

# for word, count in wordCounts.items():
#     cleanWord = word.encode('ascii', 'ignore')
#     if (cleanWord):
#         print(cleanWord.decode() + " " + str(count))