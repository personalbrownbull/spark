# spark-submit 00_keyvalue.py
from pyspark import SparkConf, SparkContext
import collections
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/fakefriends/fakefriends.csv"

# Dbg
dbg = 1

def parseLine(line):
  fields = line.split(',')
  age = int(fields[2])
  numFriends = int(fields[3])
  return (age, numFriends)

# Spark Config
conf = SparkConf().setMaster("local").setAppName("RatingsHistogram")
sc = SparkContext(conf = conf)

# INPUT
lines = sc.textFile(inputFile)
''' i.e.
0,Will,33,385
1,Jean-Luc,26,2
2,Hugh,55,221
'''

## PARSE
rdd = lines.map(parseLine)
if dbg: printRDDHead(rdd, stage = "rdd")
''' i.e.
rdd[0]: (33, 385)
rdd[1]: (26, 2)
'''

# MAIN
## CREATE KEY VALUE PAIRS
mapByAge = rdd.mapValues(lambda x: (x, 1))
if dbg: printRDDHead(mapByAge, stage = "mapByAge")
''' i.e.
mapByAge[0]: (33, (385, 1))
mapByAge[1]: (26, (2, 1))
'''

## AGGREGATE # OF FRIENDS PER AGE
totalsByAge = mapByAge.reduceByKey(lambda x, y: (x[0] + y[0], x[1] + y[1]))
if dbg: printRDDHead(totalsByAge, stage = "totalsByAge")
''' i.e.
totalsByAge[0]: (33, (3904, 12))
totalsByAge[1]: (26, (4115, 17))
'''

## GET AVG OF FRIENDS PER AGE
averagesByAge = totalsByAge.mapValues(lambda x: x[0] / x[1])
if dbg: printRDDHead(averagesByAge, stage = "averagesByAge")
''' i.e.
averagesByAge[0]: (33, 325.3333333333333)
averagesByAge[1]: (26, 242.05882352941177)
'''

## OUTPUT
results = averagesByAge.collect()
if dbg: printRDDHead(results, stage = "results")
''' i.e.
results[0]: (33, 325.3333333333333)
results[1]: (26, 242.05882352941177)
'''

for result in results:
    print(result)