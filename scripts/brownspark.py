'''
Set of functions to use on spark context
Author: Brownbull - carcamo.gabriel@gmail.com
'''

def printRDDHead(rdd, stage = "", rows = 10):
  if hasattr(rdd, 'collect'):
    toPrint = rdd.collect()
  else:
    toPrint = rdd
  for idx, e in enumerate(toPrint):
    if idx < rows:
      print("{}[{}]: {}".format(stage, idx, e))
    else:
      return