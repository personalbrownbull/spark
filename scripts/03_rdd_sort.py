# spark-submit 03_sort.py
from pyspark import SparkConf, SparkContext
import collections
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/Book.txt"

# FUNCS
import re
def normalizeWords(text):
  return re.compile(r'\W+', re.UNICODE).split(text.lower())

# Dbg
dbg = 1

# Spark Config
conf = SparkConf().setMaster("local").setAppName("Wordcount")
sc = SparkContext(conf = conf)

# INPUT
lines = sc.textFile(inputFile)
# if dbg: printRDDHead(lines, stage = "lines")
# ''' i.e.
# '''

# PARSE
# words = lines.flatMap(lambda x: x.split()) # old way with out punctuation filtering
words = lines.flatMap(normalizeWords)
if dbg: printRDDHead(words, stage = "words")
''' i.e.
words[0]: self
words[1]: employment
'''

# Add counter and aggregate by word
wordCounts = words.map(lambda x: (x, 1)).reduceByKey(lambda x, y: x + y)
if dbg: printRDDHead(wordCounts, stage = "wordCounts")
''' i.e.
wordCounts[0]: ('self', 111)
wordCounts[1]: ('employment', 75)
'''

# Reorder cols and SORT by key
wordCountsSorted = wordCounts.map(lambda x: (x[1], x[0])).sortByKey(ascending = False)
if dbg: printRDDHead(wordCountsSorted, stage = "wordCountsSorted")
''' i.e.
wordCountsSorted[0]: (1878, 'you')
wordCountsSorted[1]: (1828, 'to')
'''