# spark-submit 04_countByKey.py
from pyspark import SparkConf, SparkContext
import collections
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/customer-orders.csv"

# FUNCS
def parseLine(line):
  fields = line.split(',')
  customerID = int(fields[0])
  Amnt = float(fields[2])
  return (customerID, Amnt)

# Dbg
dbg = 1

# Spark Config
conf = SparkConf().setMaster("local").setAppName("MinTemperatures")
sc = SparkContext(conf = conf)

# INPUT
lines = sc.textFile(inputFile)
''' i.e.
44,8602,37.19
35,5368,65.89
'''

## PARSE
rdd = lines.map(parseLine)
if dbg: printRDDHead(rdd, stage = "rdd")
''' i.e.
rdd[0]: ('44', 37.19)
rdd[1]: ('35', 65.89)
'''

## AGGREGATE
customerAmnts = rdd.reduceByKey(lambda x, y: x + y)
if dbg: printRDDHead(customerAmnts, stage = "customerAmnts")
''' i.e.
customerAmnts[0]: ('44', 4756.8899999999985)
customerAmnts[1]: ('35', 5155.419999999999)
'''

## SORT by Amount
sortedCons = customerAmnts.sortBy(lambda x: x[1], ascending = False)
if dbg: printRDDHead(sortedCons, stage = "sortedCons")
''' i.e.
sortedCons[0]: (68, 6375.449999999997)
sortedCons[1]: (73, 6206.199999999999)
'''