# spark-submit 05_df_keyvalue.py
from pyspark.sql import SparkSession
from pyspark.sql import Row
from brownspark import *

# VARS
inputFile = "file:///D:/Reference/ud_sparkpy/data/fakefriends/fakefriends.csv"

# Dbg
dbg = 1

def mapper(line):
    fields = line.split(',')
    return Row(
      ID=int(fields[0]), 
      name=str(fields[1].encode("utf-8")),
      age=int(fields[2]), 
      numFriends=int(fields[3])
    )

# Spark Config
# Create a SparkSession
spark = SparkSession.builder.appName("SparkSQL").getOrCreate()

lines = spark.sparkContext.textFile(inputFile)
people = lines.map(mapper)

# INIT TABLE
# Infer the schema, and register the DataFrame as a table.
schemaPeople = spark.createDataFrame(people).cache()
schemaPeople.createOrReplaceTempView("people")
if dbg: printRDDHead(schemaPeople, stage = "schemaPeople")
''' i.e.
schemaPeople[0]: Row(ID=0, age=33, name="b'Will'", numFriends=385)  
schemaPeople[1]: Row(ID=1, age=26, name="b'Jean-Luc'", numFriends=2)
'''

# BATCH SELECT
# SQL can be run over DataFrames that have been registered as a table.
teenagers = spark.sql("SELECT * FROM people WHERE age >= 13 AND age <= 19")
if dbg: printRDDHead(teenagers, stage = "teenagers")
''' i.e.
teenagers[0]: Row(ID=21, age=19, name="b'Miles'", numFriends=268)
teenagers[1]: Row(ID=52, age=19, name="b'Beverly'", numFriends=269)
'''

# ONLINE SELECT
# We can also use functions instead of SQL queries:
schemaPeople.groupBy("age").count().orderBy("age", ascending = False).show(n = 5)
''' i.e.
+---+-----+
|age|count|
+---+-----+
| 69|   10|
| 68|   10|
| 67|   16|
| 66|    9|
| 65|    5|
+---+-----+
'''

spark.stop()
